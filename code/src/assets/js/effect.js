import $ from 'jquery'

$('#fb-accordion').on('hide.bs.collapse show.bs.collapse', function (e) {
  $(e.target)
    .prev()
    .find('i:last-child')
    .toggleClass('fa-angle-down fa-angle-up')
})
var arrayInputSearchs = {
  ddinputmunicipio: ['ddmunicipio'],
  ddinputcolonia: ['ddcolonia']
}

$.each(arrayInputSearchs, function (k, v) {
  $('#' + k).keyup(function () {
    filterFunction('#' + k, '#' + v[0])
  })
})

var filterFunction = function (inputSearch, panelSearch) {
  var input, filter, i, txtValue, div
  input = $(inputSearch)
  filter = input.val().toUpperCase()
  div = []
  div = $(panelSearch).find('a')
  for (i = 0; i < div.length; i++) {
    txtValue = div[i].textContent || div[i].innerText
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      div[i].style.display = ''
    } else {
      div[i].style.display = 'none'
    }
  }
}
