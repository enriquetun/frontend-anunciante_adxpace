import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/store/api'
import env from '@/.env'
import moment from 'moment'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    apiRoot: env.apiRoot,
    authenticated: false,
    clientes: false,
    currentCliente: {},
    currentRelacion: {},
    cliente_relaciones: false,
    relacion_inventarios: [],
    csrfmiddlewaretoken: '',
    data_espacios: false,
    data_locales: false,
    data_institucionales: false,
    data_geo_localidad: false,
    geo_localidad_input: false,
    prueba_initmap: false,
    data_oootipo: [],
    data_url_espacio: '',
    data_url_local: '',
    data_url_institucional: ''
  },
  getters: {
    'inventarios': function (state) {
      var relacionInventarios = state.relacion_inventarios
      var inventarios = []
      for (var n = 0; n < relacionInventarios.length; n++) {
        var relacion = relacionInventarios[n]
        var inventario = {}

        if (relacion.relacion_tipo === 'E') {
          inventario.pk = relacion.espacio.pk
          inventario.pk_relacion = relacion.pk
          inventario.valorN = n
          inventario.latitud = relacion.espacio.geo_localidad.latitud
          inventario.longitud = relacion.espacio.geo_localidad.longitud
          inventario.foto = relacion.espacio.foto
          inventario.relacion_tipo = 'Espacio'
          inventario.name = relacion.espacio.nombre
          inventario.giro = relacion.espacio.giro.designacion
          inventario.tipo = relacion.espacio.tipo.designacion
          inventarios.push(inventario)
        } else if (relacion.relacion_tipo === 'L') {
          inventario.pk = relacion.local.pk
          inventario.pk_relacion = relacion.pk
          inventario.valorN = n
          inventario.latitud = relacion.local.geo_localidad.latitud
          inventario.longitud = relacion.local.geo_localidad.longitud
          inventario.foto = relacion.local.foto
          inventario.relacion_tipo = 'Local'
          inventario.name = relacion.local.nombre
          inventario.calle = relacion.local.direccion.calle
          inventario.colonia = relacion.local.direccion.colonia
          inventario.municipio = relacion.local.direccion.municipio
          inventarios.push(inventario)
        } else {
          inventario.n = n
          inventario.pk_relacion = relacion.pk
          inventario.pk = relacion.institucional.pk
          inventario.latitud = relacion.institucional.geo_localidad.latitud
          inventario.longitud = relacion.institucional.geo_localidad.longitud
          if (relacion.institucional.data.foto) {
            inventario.foto = relacion.institucional.data.foto[0]
          } else {
            inventario.foto = null
          }
          inventario.relacion_tipo = 'Institucional'
          inventario.name = relacion.institucional.data.empresa
          inventario.tipo = relacion.institucional.data.tipo
          inventario.calificacion = relacion.institucional.data.calificacion
          inventarios.push(inventario)
        }
      }
      return inventarios
    }
  },
  actions: {
    async comun (store) {
      await store.dispatch('giros')
      await store.dispatch('tipos')
      await store.dispatch('materiales')
      await store.dispatch('ooh_tipos')
    },
    async giros ({state, commit}) {
      var url = state.apiRoot + '/comun/giros/'
      return api.get(url)
        .then((response) => commit('GIROS', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async tipos ({state, commit}) {
      var url = state.apiRoot + '/comun/tipos/'
      return api.get(url)
        .then((response) => commit('TIPOS', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async materiales ({state, commit}) {
      var url = state.apiRoot + '/comun/materiales/'
      return api.get(url)
        .then((response) => commit('MATERIALES', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async ooh_tipos ({state, commit}) {
      var url = state.apiRoot + '/comun/ooh_tipos/'
      return api.get(url)
        .then((response) => commit('OOOTIPO', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async search_point ({state, commit}, direccion) {
      var url = state.apiRoot + '/comun/direccion/?direccion=' + direccion
      return api.get(url)
        .then((response) => commit('RESPONSE_POIN', response.body))
        .catch((error) => commit('API_FAIL', error))
    },
    async cliente ({state, commit}) {
      var url = state.apiRoot + '/anunciante/cliente/'
      return api.get(url)
        .then((response) => commit('GET_CLIENTE', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async get_anunciante_relaciones ({state, commit}, pk) {
      var url = state.apiRoot + '/anunciante/relaciones/'
      return api.get(url)
        .then((response) => commit('GET_ANUNCIANTE_RELACIONES', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async get_relacion_inventarios ({state, commit}, pk) {
      var url = state.apiRoot + '/anunciante/relaciones/inventarios/' + pk
      return api.get(url)
        .then((response) => commit('GET_RELACIONES_INVENTARIOS', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async get_relacion ({state, commit}, pk) {
      function getRelacion (clienteRelaciones) {
        return clienteRelaciones.pk === pk
      }
      var currentRelacion = state.cliente_relaciones.find(getRelacion)
      commit('SET_CURRENT_RELACION', currentRelacion)
    },
    // Posts
    async post_inventario ({state, commit}, postInventario) {
      var url = state.apiRoot + '/anunciante/relaciones/inventarios/' + state.currentRelacion.pk
      return api.post_json(url, postInventario)
    },
    // Delete relacion inventarios
    async delete_relacion_inventario ({state, commit}, data) {
      var url = state.apiRoot + '/anunciante/relaciones/inventarios/' + data.pk_relacion_cliente + '?pk=' + data.pk_inventario
      return api.delete(url)
        .then((response) => commit('RESPONSE_DELETE', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async delete_campana ({state, commit}, pkCampania) {
      var url = state.apiRoot + '/anunciante/relaciones/?pk=' + pkCampania
      return api.delete(url)
        .then((response) => commit('RESPONSE_DELETE', response))
        .catch((error) => commit('API_FAIL', error))
    },
    // ---------------- search Espacio ----------
    async searchGeolocalidad ({state, commit}, search) {
      var url = state.apiRoot + '/comun/direccion/' + search
      return api.get(url)
        .then((response) => commit('RESPONSE_GEOLOCALIDAD_INPUT', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async searchEspacios ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + '/anunciante/espacios/' + search
      }
      store.commit('URLDATA', url)
      return api.get(url)
        .then((response) => {
          var dataEspacios = response.body
          for (let i = 0; i < dataEspacios.results.length; i++) {
            dataEspacios.results[i].fecha_de_registro = moment(dataEspacios.results[i].fecha_de_registro)
          }
          store.commit('ESPACIOS_SEARCH', dataEspacios)
        }).catch((error) => commit('API_FAIL', error))
    },
    async searchLocales ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + '/anunciante/locales/' + search
      }
      store.commit('URLDATALOCAL', url)
      return api.get(url)
        .then((response) => {
          var dataLocales = response.body
          for (var n = 0; n < dataLocales.results.length; n++) {
            dataLocales.results[n].fecha_de_registro = moment(dataLocales.results[n].fecha_de_registro)
          }
          store.commit('LOCALES_SEARCH', dataLocales)
        }).catch((error) => commit('API_FAIL', error))
    },
    async searchInstitucionales ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + '/anunciante/espacios_institucionales/' + search
      }
      store.commit('URLDATAINSTITUCIONAL', url)
      return api.get(url)
        .then((response) => {
          store.commit('INSTITUCIONALES_SEARCH', response)
        }).catch((error) => commit('API_FAIL', error))
    },
    async post_campaña ({state, commit}, data) {
      var url = state.apiRoot + '/anunciante/relaciones/'
      return api.post_json(url, data)
        .then((response) => {
          commit('RESPONSE_ADD_SUCURSAL', response)
        }).catch((error) => commit('API_FAIL', error))
    }
  },
  mutations: {
    URLDATA: function (state, url) {
      state.data_url_espacio = url
    },
    URLDATALOCAL: function (state, url) {
      state.data_url_local = url
    },
    URLDATAINSTITUCIONAL: function (state, url) {
      state.data_url_institucional = url
    },
    GIROS: function (state, giros) {
      state.data_giros = giros.body
    },
    TIPOS: function (state, tipos) {
      state.data_tipos = tipos.body
    },
    MATERIALES: function (state, materiales) {
      state.data_materiales = materiales
    },
    OOOTIPO: function (state, tipo) {
      state.data_oootipo = tipo.body
    },
    SET_CURRENT_RELACION: function (state, currentRelacion) {
      // console.log(currentRelacion)
      state.currentRelacion = currentRelacion
    },
    GET_CLIENTE: function (state, response) {
      state.cliente = response.body
    },
    GET_ANUNCIANTE_RELACIONES: function (state, response) {
      state.cliente_relaciones = response.body
    },
    GET_RELACIONES_INVENTARIOS: function (state, response) {
      state.relacion_inventarios = response.body
    },
    RESPONSE_ADD_SUCURSAL: function (state, response) {
      state.response_sucursal = response.body.pk
    },
    ESPACIOS_SEARCH: function (state, dataEspacios) {
      state.data_espacios = dataEspacios
    },
    LOCALES_SEARCH: function (state, dataLocales) {
      // console.log(dataLocales)
      state.data_locales = dataLocales
    },
    INSTITUCIONALES_SEARCH: function (state, response) {
      // console.log(response.body)
      state.data_institucionales = response.body
    },
    GEO_LOCALIDAD_RESET: function (state) {
      state.data_geo_localidad = []
    },
    GEO_LOCALIDAD: function (state, response) {
      state.data_geo_localidad = response
    },
    RESET_TABLE: function (state, response) {
      state.data_espacios = []
      state.data_locales = []
      state.data_institucionales = []
    },
    RESET_POIN: function (state) {
      state.prueba_initmap = []
    },
    RESPONSE_POIN: function (state, response) {
      state.prueba_initmap = response
    },
    RESPONSE_GEOLOCALIDAD_INPUT: function (state, response) {
      state.geo_localidad_input = response.body
    },
    RESPONSE_DELETE: function (state, response) {
    },
    API_FAIL (state, error) {
      console.error(error)
    }
  }
})

export default store
