import Vue from 'vue'
import Router from 'vue-router'
import TablaSucursales from '@/components/TablaSucursales'
import TablaSucursalEspacios from '@/components/TablaSucursalEspacios'
import ClienteAddCampaña from '@/components/ClienteAddCampaña'
import ClienteEditCampania from '@/components/ClienteEditCampania'
import AnuncianteEspacios from '@/components/AnuncianteEspacios'
import tablaLocales from '@/components/tablaLocales'
import tablaEspacios from '@/components/tablaEspacios'
import tablaEspaciosInstitucionales from '@/components/tablaEspaciosInstitucionales'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'TablaSucursales',
      component: TablaSucursales
    },
    {
      path: '/cliente/sucursal/espacios',
      name: 'SucursalEspacios',
      component: TablaSucursalEspacios
    },
    {
      path: '/anunciante/add/campaña',
      name: 'ClienteAddCampaña',
      component: ClienteAddCampaña
    },
    {
      path: '/anunciante/add/campania',
      name: 'ClienteEditCampania',
      component: ClienteEditCampania
    },
    {
      path: '/anunciante/add/espacios',
      name: 'AnuncianteEspacios',
      component: AnuncianteEspacios
    },
    {
      path: '/anunciante/sucursal/espacios/locales',
      name: 'tablaLocales',
      component: tablaLocales
    },
    {
      path: '/anunciante/sucursal/espacios/espacios',
      name: 'TablaEspacios',
      component: tablaEspacios
    },
    {
      path: '/anunciante/sucursal/espacios/institucionales',
      name: 'tablaEspaciosInstitucionale',
      component: tablaEspaciosInstitucionales
    }
  ]
})
