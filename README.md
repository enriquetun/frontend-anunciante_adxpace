# Frontend Sales

ADXPACE code for sales functionality

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
docker
docker-compose
```

### Installing

Clone the repo.

cd into the project folder.

Create  ```/code/db.json file ``` and store some json data:

```
{
  "posts": [
    { "id": 1, "title": "json-server", "author": "typicode" },
    { "id": 2, "title": "example", "author": "luis morales" },
    { "id": 3, "title": "just another example", "author": "enrique tun" },
    { "id": 4, "title": "yet another example", "author": "daniela florencia" }
  ],
  "comments": [
    { "id": 1, "body": "some comment", "postId": 1 }
  ],
  "profile": { "name": "typicode" }
}
```

Now run script ```sh start.sh```

This will create two containers, one running frontend suite and a
json-server to host some fake data.

Run:
```
docker-compose ps
```
You should have two containers up and running.  If any of the containers
is exited run:

```
docker-compose stop
```

Run:
```
docker-compose up frontend-vendedor
```

Watch for errors. If any, comment-out the next line in file ```docker-compose.yaml``` under service frontend-vendedor:
```
command: npm run dev
```
Again, run:
```
docker-compose up frontend-vendedor
```
ssh into that container with a new terminal running script:```container_bash.sh ```

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
